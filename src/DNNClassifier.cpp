/*
 * DNN Classifier.
 * Please note that this classifier actually outputs 7 discriminator values
 * simultaneously. For the moment, the DNN's only work in the SL channel.
 * They can be interpreted as a classification probability as they sum up to 1.
 * Classes (order is important!): ttH, ttbb, ttb, tt2b, ttcc, ttlf, other
 */

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

DNNClassifier::DNNClassifier(std::string version)
    : _nFeatures4(0)
    , _nFeatures5(0)
    , _nFeatures6(0)
    , _inputName("inp")
    , _outputName("outp")
    , _dropoutName("keep_prob")
    , _csvCut(0.8)
{
    // set feature numbers based in the version
    if (version == "v2")
    {
        _nFeatures4 = 39;
        _nFeatures5 = 44;
        _nFeatures6 = 49;
    }
    else
    {
        throw std::runtime_error("unknown version: " + version);
    }

    // determine some local paths
    std::string cmsswBase = std::string(getenv("CMSSW_BASE"));
    std::string tfdeployBase = cmsswBase + "/python/TTH/CommonClassifier";
    std::string modelsBase = cmsswBase + "/src/TTH/CommonClassifier/data/dnnmodels_" + version;
    std::string modelFile4 = modelsBase + "/model_4j.pkl";
    std::string modelFile5 = modelsBase + "/model_5j.pkl";
    std::string modelFile6 = modelsBase + "/model_6j.pkl";

    // start python
    PyEval_InitThreads();
    Py_Initialize();

    // initialize the python main object, load the script
    PyObject* pyMainModule = PyImport_AddModule("__main__");
    PyObject* pyMainDict = PyModule_GetDict(pyMainModule);
    _pyContext = PyDict_Copy(pyMainDict);
    PyRun_String(evalScript.c_str(), Py_file_input, _pyContext, _pyContext);

    // load the tfdeploy models
    PyObject* pySetup = PyDict_GetItemString(_pyContext, "setup");
    PyObject* pyModelFiles = PyTuple_New(3);
    PyTuple_SetItem(pyModelFiles, 0, PyString_FromString(modelFile4.c_str()));
    PyTuple_SetItem(pyModelFiles, 1, PyString_FromString(modelFile5.c_str()));
    PyTuple_SetItem(pyModelFiles, 2, PyString_FromString(modelFile6.c_str()));
    PyObject* pyArgs = PyTuple_New(5);
    PyTuple_SetItem(pyArgs, 0, PyString_FromString(tfdeployBase.c_str()));
    PyTuple_SetItem(pyArgs, 1, pyModelFiles);
    PyTuple_SetItem(pyArgs, 2, PyString_FromString(_inputName.c_str()));
    PyTuple_SetItem(pyArgs, 3, PyString_FromString(_outputName.c_str()));
    PyTuple_SetItem(pyArgs, 4, PyString_FromString(_dropoutName.c_str()));

    PyObject* pyResult = PyObject_CallObject(pySetup, pyArgs);
    if (pyResult == NULL)
    {
        if (PyErr_Occurred() != NULL)
        {
            PyErr_PrintEx(0);
        }
        throw runtime_error("an error occured while loading the tfdeploy models");
    }

    // store the evaluation function and prepare args
    // the "+ 1" is due to the model number being the first argument in the eval function
    _pyEval = PyDict_GetItemString(_pyContext, "eval");
    _pyEvalArgs4 = PyTuple_New(_nFeatures4 + 1);
    _pyEvalArgs5 = PyTuple_New(_nFeatures5 + 1);
    _pyEvalArgs6 = PyTuple_New(_nFeatures6 + 1);
}

DNNClassifier::~DNNClassifier()
{
    // cleanup python objects
    Py_DECREF(_pyEvalArgs4);
    Py_DECREF(_pyEvalArgs5);
    Py_DECREF(_pyEvalArgs6);
    Py_DECREF(_pyEval);
    Py_DECREF(_pyContext);
    Py_Finalize();
}

void DNNClassifier::evaluate(const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const TLorentzVector& lepton, const TLorentzVector& met,
    DNNOutput& dnnOutput)
{
    size_t nJets = jets.size();
    size_t modelNum;
    PyObject* pyEvalArgs = NULL;
    if (nJets < 4)
    {
        throw runtime_error("no DNN classifier existing for < 4 jets");
    }
    else if (nJets == 4)
    {
        pyEvalArgs = _pyEvalArgs4;
        modelNum = 0;
    }
    else if (nJets == 5)
    {
        pyEvalArgs = _pyEvalArgs5;
        modelNum = 1;
    }
    else
    {
        pyEvalArgs = _pyEvalArgs6;
        modelNum = 2;
    }

    // modelNum is at pos 0
    PyTuple_SetItem(pyEvalArgs, 0, PyInt_FromSize_t(modelNum));

    // fill features into the py tuple
    _fillFeatures(pyEvalArgs, jets, jetCSVs, lepton, met);

    // evaluate
    PyObject* pyList = PyObject_CallObject(_pyEval, pyEvalArgs);

    // fill the dnnOutput
    dnnOutput.values.resize(7);
    for (size_t i = 0; i < 7; i++)
    {
        dnnOutput.values[i] = PyFloat_AsDouble(PyList_GetItem(pyList, i));
    }

    Py_DECREF(pyList);
}

DNNOutput DNNClassifier::evaluate(const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const TLorentzVector& lepton, const TLorentzVector& met)
{
    DNNOutput dnnOutput;
    evaluate(jets, jetCSVs, lepton, met, dnnOutput);
    return dnnOutput;
}

void DNNClassifier::_fillFeatures(PyObject* pyEvalArgs, const std::vector<TLorentzVector>& jets,
    const std::vector<double>& jetCSVs, const TLorentzVector& lepton, const TLorentzVector& met)
{
    size_t idx = 1;

    // low-level jet features: pt, eta, phi, mass, csv
    for (size_t i = 0; i < min(jets.size(), (size_t)6); i++)
    {
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[i].Pt()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[i].Eta()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[i].Phi()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jets[i].Mag()));
        PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(jetCSVs[i]));
    }

    // low-level lepton features: pt, eta, phi, mass
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Pt()));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Eta()));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Phi()));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(lepton.Mag()));

    // low-level met features: pt, phi
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(met.Pt()));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(met.Phi()));

    // split jets into b jets and light jets
    // store pointers to avoid performance drawbacks due to vector copying
    std::vector<const TLorentzVector*> allJets;
    std::vector<const TLorentzVector*> bJets;
    std::vector<double> bCSVs;
    std::vector<const TLorentzVector*> lJets;
    std::vector<double> lCSVs;
    for (size_t i = 0; i < jets.size(); i++)
    {
        allJets.push_back(&jets[i]);
        if (jetCSVs[i] >= _csvCut)
        {
            bJets.push_back(&jets[i]);
            bCSVs.push_back(jetCSVs[i]);
        }
        else
        {
            lJets.push_back(&jets[i]);
            lCSVs.push_back(jetCSVs[i]);
        }
    }

    // Ht
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(getHt(allJets)));

    // min and max dR between jets
    double minDRJets, maxDRJets;
    getMinMaxDR(allJets, minDRJets, maxDRJets);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRJets));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDRJets));

    // min and max dR between light jets
    double minDRLJets, maxDRLJets;
    getMinMaxDR(lJets, minDRLJets, maxDRLJets);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRLJets));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDRLJets));

    // min and max dR between b jets
    double minDRBJets, maxDRBJets;
    getMinMaxDR(bJets, minDRBJets, maxDRBJets);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRBJets));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDRBJets));

    // min and max dR between b jets and the lepton
    double minDRBJetsLep, maxDRBJetsLep;
    getMinMaxDR(bJets, &lepton, minDRBJetsLep, maxDRBJetsLep);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(minDRBJetsLep));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(maxDRBJetsLep));

    // jet centrality
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(getCentrality(allJets)));

    // jet aplanarity, sphericity and transverse sphericity
    double ev1, ev2, ev3;
    getSphericalEigenValues(allJets, ev1, ev2, ev3);
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * ev3));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(3. / 2. * (ev2 + ev3)));
    double tSphericity = ev2 == 0 ? 0 : (2. * ev2 / (ev1 + ev2));
    PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(tSphericity));

    // Fox-Wolfram moments
    // disabled for the moment, see https://gitlab.cern.ch/ttH/CommonClassifier/issues/1
    // double fw1, fw2, fw3, fw4, fw5;
    // _bdtVars.getFox(jets, fw1, fw2, fw3, fw4, fw5);
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw1));
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw2));
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw3));
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw4));
    // PyTuple_SetItem(pyEvalArgs, idx++, PyFloat_FromDouble(fw5));
}

double DNNClassifier::getHt(const std::vector<const TLorentzVector*>& lvecs) const
{
    double ht = 0;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        ht += lvecs[i]->Pt();
    }
    return ht;
}

void DNNClassifier::getMinMaxDR(
    const std::vector<const TLorentzVector*>& lvecs, double& minDR, double& maxDR) const
{
    maxDR = lvecs.size() == 0 ? -1 : 0.;
    minDR = lvecs.size() == 0 ? -1 : 100.;
    if (lvecs.size() >= 2)
    {
        for (size_t i = 0; i < lvecs.size() - 1; i++)
        {
            for (size_t j = i + 1; j < lvecs.size(); j++)
            {
                double dR = lvecs[i]->DeltaR(*lvecs[j]);
                if (dR > maxDR)
                {
                    maxDR = dR;
                }
                if (dR < minDR)
                {
                    minDR = dR;
                }
            }
        }
    }
}

void DNNClassifier::getMinMaxDR(const std::vector<const TLorentzVector*>& lvecs,
    const TLorentzVector* lvec, double& minDR, double& maxDR) const
{
    maxDR = lvecs.size() == 0 ? -1 : 0.;
    minDR = lvecs.size() == 0 ? -1 : 100.;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        double dR = lvecs[i]->DeltaR(*lvec);
        if (dR > maxDR)
        {
            maxDR = dR;
        }
        if (dR < minDR)
        {
            minDR = dR;
        }
    }
}

double DNNClassifier::getCentrality(const std::vector<const TLorentzVector*>& lvecs) const
{
    double sumPt = 0.;
    double sumP = 0.;
    for (size_t i = 0; i < lvecs.size(); i++)
    {
        sumPt += lvecs[i]->Pt();
        sumP += lvecs[i]->P();
    }
    return sumPt / sumP;
}

void DNNClassifier::getSphericalEigenValues(
    const std::vector<const TLorentzVector*>& lvecs, double& ev1, double& ev2, double& ev3) const
{
    TMatrixDSym momentumMatrix(3);
    double p2Sum = 0.;

    for (size_t i = 0; i < lvecs.size(); i++)
    {
        double px = lvecs[i]->Px();
        double py = lvecs[i]->Py();
        double pz = lvecs[i]->Pz();

        // fill the matrix
        momentumMatrix(0, 0) += px * px;
        momentumMatrix(0, 1) += px * py;
        momentumMatrix(0, 2) += px * pz;
        momentumMatrix(1, 0) += py * px;
        momentumMatrix(1, 1) += py * py;
        momentumMatrix(1, 2) += py * pz;
        momentumMatrix(2, 0) += pz * px;
        momentumMatrix(2, 1) += pz * py;
        momentumMatrix(2, 2) += pz * pz;

        // add 3 momentum squared to sum
        p2Sum += px * px + py * py + pz * pz;
    }

    // normalize each element by p2Sum
    if (p2Sum != 0.)
    {
        for (size_t i = 0; i < 3; i++)
        {
            for (size_t j = 0; j < 3; j++)
            {
                momentumMatrix(i, j) = momentumMatrix(i, j) / p2Sum;
            }
        }
    }

    // calculatate eigen values via eigen vectors
    TMatrixDSymEigen eig(momentumMatrix);
    TVectorD ev = eig.GetEigenValues();

    // some checks due to limited precision of TVectorD
    ev1 = fabs(ev[0]) < 0.00000000000001 ? 0 : ev[0];
    ev2 = fabs(ev[1]) < 0.00000000000001 ? 0 : ev[1];
    ev3 = fabs(ev[2]) < 0.00000000000001 ? 0 : ev[2];
}

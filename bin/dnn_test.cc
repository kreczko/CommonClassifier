/*
 * DNN classification test.
 */

#include <iostream>
#include <limits>
#include <sys/time.h>

#include "TTH/CommonClassifier/interface/DNNClassifier.h"

#ifndef EPSILON
#define EPSILON 1.0e-6
#endif

const TLorentzVector makeVector(double pt, double eta, double phi, double mass)
{
    TLorentzVector lv;
    lv.SetPtEtaPhiM(pt, eta, phi, mass);
    return lv;
}

void test_DNN_v2();

int main()
{
  test_DNN_v2();
}

void test_DNN_v2() {
    // set precision of numbers in cout
    std::cout.precision(8);

    // setup the dnn classifier
    DNNClassifier dnn;

    DNNOutput output4;
    DNNOutput output5;
    DNNOutput output6;

    // model 4j
    {
        std::vector<TLorentzVector> jets
            = { makeVector(82.193317445, -1.381465673, 0.875596046, 12.354169733),
                makeVector(61.222326911, -0.624968886, 1.280390978, 7.378321245),
                makeVector(49.268722080, -1.523553133, 2.887234688, 10.714914255),
                makeVector(45.137310817, -0.522410452, 2.953480721, 4.287899215) };
        std::vector<double> jetCSVs = { 0.951796949, 0.167348146, 0.164827660, 0.804015398 };

        TLorentzVector lepton = makeVector(126.932662964, 0.030971697, -0.758532584, 0.105700001);
        TLorentzVector met = makeVector(49.720878601, 0., -2.168250561, 0.);

        // evaluate
        output4 = dnn.evaluate(jets, jetCSVs, lepton, met);
    }

    // model 5j
    {
        std::vector<TLorentzVector> jets
            = { makeVector(113.789252322, -0.750472307, -1.340483546, 11.388389050),
                makeVector(104.010172609, -1.185562849, -2.004201174, 20.797776564),
                makeVector(65.583533585, -1.123236418, 2.088073730, 12.479995542),
                makeVector(44.232577186, -1.738008976, -1.576105833, 6.927932890),
                makeVector(40.915780832, -2.206727982, 2.069944143, 5.814550891)

              };
        std::vector<double> jetCSVs
            = { 0.998467326, 0.996747077, 0.056155164, 0.838712037, -10.000000000 };

        TLorentzVector lepton = makeVector(96.684181213, -0.904775918, 0.371903986, 0.105700001);
        TLorentzVector met = makeVector(50.490451813, 0.0, 0.888747931, 0.0);

        // evaluate
        output5 = dnn.evaluate(jets, jetCSVs, lepton, met);
    }

    // model 6j
    std::vector<TLorentzVector> jets
        = { makeVector(561.853400685, -1.687604070, 1.248519063, 48.315901270),
            makeVector(317.050318074, -0.592420697, -1.641329408, 41.454653026),
            makeVector(130.993330071, -0.257405132, -2.218888283, 15.829634094),
            makeVector(90.088794331, -0.758062780, -1.199205875, 11.687776745),
            makeVector(57.793076163, 0.163249642, 2.988173246, 9.959981641),
            makeVector(45.080527604, -1.838460445, 1.761856556, 7.07615728) };
    std::vector<double> jetCSVs
        = { 0.592329562, 0.156740859, 0.998748481, 0.944896400, 0.268956393, 0.109731160 };

    TLorentzVector lepton = makeVector(45.567291260, -1.122234225, 2.685425282, 0.105700001);
    TLorentzVector met = makeVector(70.089050293, 0.0, -2.230507374, 0.0);

    // evaluate
    output6 = dnn.evaluate(jets, jetCSVs, lepton, met);

    // some output
    std::vector<DNNOutput> outputs = { output4, output5, output6 };
    for (size_t i = 0; i < outputs.size(); ++i)
    {
        std::cout << "evaluation for model " << (i + 4) << "j:" << std::endl;
        std::cout << "0 - ttH:   " << std::fixed << outputs[i].ttH() << std::endl;
        std::cout << "1 - ttbb:  " << std::fixed << outputs[i].ttbb() << std::endl;
        std::cout << "2 - ttb:   " << std::fixed << outputs[i].ttb() << std::endl;
        std::cout << "3 - tt2b:  " << std::fixed << outputs[i].tt2b() << std::endl;
        std::cout << "4 - ttcc:  " << std::fixed << outputs[i].ttcc() << std::endl;
        std::cout << "5 - ttlf:  " << std::fixed << outputs[i].ttlf() << std::endl;
        std::cout << "6 - other: " << std::fixed << outputs[i].other() << std::endl;
        std::cout << "most probable class: " << outputs[i].mostProbableClass() << std::endl;
        std::cout << std::endl;
    }

    //validate outputs
    std::vector<double> targetOutputs4 = {0.13662557, 0.09063863, 0.12108074, 0.07809219, 0.15359062, 0.20219319, 0.21777909};
    std::vector<double> targetOutputs5 = {0.26851401, 0.15041369, 0.12638351, 0.17068394, 0.11537127, 0.08704317, 0.08159041};
    std::vector<double> targetOutputs6 = {0.16316491, 0.08395188, 0.10267062, 0.1429254, 0.14195499, 0.13069865, 0.23463349};

    for (size_t i = 0; i < targetOutputs4.size(); ++i) {
      assert(fabs(targetOutputs4[i] - output4.values[i]) < EPSILON && "The DNN output for 4 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs5.size(); ++i) {
      assert(fabs(targetOutputs5[i] - output5.values[i]) < EPSILON && "The DNN output for 5 jet events is incorrect");
    }
    for (size_t i = 0; i < targetOutputs6.size(); ++i) {
      assert(fabs(targetOutputs6[i] - output6.values[i]) < EPSILON && "The DNN output for 6 jet events is incorrect");
    }

    // performance test
    std::cout << "performance test:" << std::endl;
    struct timeval tp;
    gettimeofday(&tp, NULL);
    long int t0 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    size_t n = 50000;
    for (size_t i = 0; i < n; ++i)
    {
        dnn.evaluate(jets, jetCSVs, lepton, met);
    }
    gettimeofday(&tp, NULL);
    long int t1 = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    int diff = t1 - t0;
    std::cout << n << " evalutions took " << (diff / 1000.) << " seconds" << std::endl;
    std::cout << "=> " << (((float)diff) / n) << " ms per evaluation" << std::endl;
}

#undef EPSILON

import json, ROOT, sys

infile = sys.argv[1]

inf = ROOT.TFile.Open(infile)
tt = inf.Get("tree")
for ev in tt:
    jsonev = {
        "run": int(ev.run),
        "lumi": int(ev.lumi),
        "event": int(ev.event),
        "systematic": int(ev.systematic),
        "jets": [
            [ev.jet_pt[ij], ev.jet_eta[ij], ev.jet_phi[ij], ev.jet_mass[ij], ev.jet_csv[ij]]
            for ij in range(ev.njets)
        ],
        "leps": [
            [ev.lep_pt[ij], ev.lep_eta[ij], ev.lep_phi[ij], ev.lep_mass[ij], ev.lep_charge[ij]]
            for ij in range(ev.nleps)
        ],
        "is_done": 0,
        "time_started": -1,
    }
    print json.dumps(jsonev)
